package az.atls.securitypro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class SecurityProApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecurityProApplication.class, args);
    }

}
