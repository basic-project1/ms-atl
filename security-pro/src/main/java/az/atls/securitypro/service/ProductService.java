package az.atls.securitypro.service;

import az.atls.securitypro.client.ProductClient;
import az.atls.securitypro.dao.entity.ProductEntity;
import az.atls.securitypro.model.ProductDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProductService {
    private final ProductClient productClient;

    public void create(ProductDto productDto) {
        productClient.createProduct(productDto);
    }

    public ProductDto getProduct(Long id) {
        return productClient.getAllProducts(id);
    }

    public ProductDto updateProduct(Long id, ProductEntity product) {
        return productClient.updateProduct(id, product);
    }

    public void deleteProduct(Long id) {
        productClient.deleteProduct(id);
    }

}
