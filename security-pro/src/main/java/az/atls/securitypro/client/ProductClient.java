package az.atls.securitypro.client;

import az.atls.securitypro.dao.entity.ProductEntity;
import az.atls.securitypro.model.ProductDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "ms-pro", url = "http://localhost:8084/products")
public interface ProductClient {
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    void createProduct(@RequestBody ProductDto productDto);

    @GetMapping("/get-products/{id}")
    @ResponseStatus(HttpStatus.OK)
    ProductDto getAllProducts(@PathVariable Long id);

    @PutMapping("/update-products/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    ProductDto updateProduct(@PathVariable Long id, @RequestBody ProductEntity productEntity);

    @DeleteMapping("/delete-product/{id}")
    @ResponseStatus(HttpStatus.OK)
    void deleteProduct(@PathVariable Long id);
}
