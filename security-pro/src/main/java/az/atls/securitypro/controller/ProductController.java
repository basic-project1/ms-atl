package az.atls.securitypro.controller;

import az.atls.securitypro.dao.entity.ProductEntity;
import az.atls.securitypro.model.ProductDto;
import az.atls.securitypro.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/main-product")
@RequiredArgsConstructor
public class ProductController {
    private final ProductService productService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createProduct(@RequestBody ProductDto productDto) {
        productService.create(productDto);
    }

    @GetMapping("/get-product-with-id/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ProductDto getProduct(@PathVariable Long id) {
        return productService.getProduct(id);

    }
    @PutMapping("/update-product-with-id/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public ProductDto updateProduct(@PathVariable Long id, @RequestBody ProductEntity productEntity){
         return productService.updateProduct(id,productEntity);
    }
    @DeleteMapping("/delete-product-with-id/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteProdcut(@PathVariable Long id){
        productService.deleteProduct(id);
    }

}
