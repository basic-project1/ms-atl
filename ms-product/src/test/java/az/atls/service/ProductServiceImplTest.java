package az.atls.service;

import az.atls.dao.entity.ProductEntity;
import az.atls.dao.repo.ProductRepository;
import az.atls.exception.NoSuchProduct;
import az.atls.model.dto.ProductDto;
import az.atls.service.impl.ProductServiceImpl;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


@SpringBootTest
public class ProductServiceImplTest {
    @InjectMocks
    private ProductServiceImpl productService;
    @Mock
    private ProductRepository productRepository;
    private static BigDecimal bigDecimal = new BigDecimal("15");

    @Test
    public void createProduct_Test() {
        // Prepare test data

        ProductDto productDto = ProductDto.builder().id(1L)
                .productName("Computer").count(1).price(bigDecimal).build();
        ProductEntity productEntity = ProductEntity.builder().id(1l)
                .productName("Computer").count(1).price(bigDecimal).build();
        when(productRepository.save(any(ProductEntity.class))).thenReturn(productEntity);
        // Perform the test
        productService.createProduct(productDto);
        assertEquals(1L, productDto.getId());
        verify(productRepository, times(1)).save(any(ProductEntity.class));


    }

    @Test
    public void deleteProduct_Test() {
        Long productID = 1l;
        productService.deleteProduct(productID);
        verify(productRepository, times(1)).deleteById(productID);

    }

    @Test
    public void getProductByExistingId_Test() {
        Long productId = 1L;
        BigDecimal bigDecimal = new BigDecimal("16.0");
        ProductEntity productEntity = ProductEntity.builder().id(1l)
                .productName("Computer").count(1)
                .price(bigDecimal).build();
        when(productRepository.findById(productId)).thenReturn(Optional.of(productEntity));
        ProductDto productDto = productService.getProductById(productId);
        assertNotNull(productDto);
        assertEquals(productId, productDto.getId());
        assertEquals("Computer", productDto.getProductName());
        verify(productRepository, times(1)).findById(productId);

    }

    @Test
    public void getProductByNonExistingId_Test() {
        Long id = 1L;
        when(productRepository.findById(id)).thenReturn(Optional.empty());
        assertThrows(NoSuchProduct.class, () -> productService.getProductById(id));
        verify(productRepository, times(1)).findById(id);
    }

    @Test
    public void getAllProducts_Test() {
        List<ProductEntity> productEntities = Arrays.asList(
                new ProductEntity(1l, "Computer", 1, bigDecimal),
                new ProductEntity(2l, "Camera", 2, bigDecimal)
        );
        when(productRepository.findAll()).thenReturn(productEntities);
        List<ProductDto> productDtoList = productService.getAllProducts();
        assertNotNull(productDtoList);
        assertEquals(2, productDtoList.size());
        assertEquals(1L, productDtoList.get(0).getId());
        assertEquals("Computer", productDtoList.get(0).getProductName());
        assertEquals(2L, productDtoList.get(1).getId());
        assertEquals("Camera", productDtoList.get(1).getProductName());
        verify(productRepository, times(1)).findAll();


    }

    @Test
    public void updateProduct_Test() {
        Long productId = 1L;
        ProductEntity product = ProductEntity.builder()
                .productName("Camera").count(2).price(bigDecimal).build();
        ProductEntity productFromDB = ProductEntity.builder()
                .id(1L).productName("Computer").count(1).price(bigDecimal).build();
        when(productRepository.findById(productId)).thenReturn(Optional.of(productFromDB));
        ProductDto productDto = productService.updateProduct(productId, product);
        when(productRepository.save(productFromDB)).thenReturn(productFromDB);

        assertNotNull(productDto);
        assertEquals(productFromDB.getId(),productDto.getId());
        assertEquals(productFromDB.getProductName(),productDto.getProductName());
        assertEquals(product.getProductName(),productDto.getProductName());
        assertEquals(productFromDB.getCount(),2);
        verify(productRepository,times(1)).save(any(ProductEntity.class));


    }


}

