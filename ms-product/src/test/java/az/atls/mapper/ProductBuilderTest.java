package az.atls.mapper;

import az.atls.dao.entity.ProductEntity;
import az.atls.model.dto.ProductDto;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static az.atls.mapper.ProductBuilder.PRODUCT_BUILDER;
import static org.junit.jupiter.api.Assertions.assertEquals;


public class ProductBuilderTest {
    public static final BigDecimal price = new BigDecimal("15.0");

    @Test
    public void test_buildDto() {
        ProductEntity product = ProductEntity.builder()
                .id(1l)
                .productName("Camera")
                .count(2)
                .price(price)
                .build();
        ProductDto productDto = PRODUCT_BUILDER.buildDto(product);
        assertEquals(product.getId(), productDto.getId());
        assertEquals(product.getProductName(), productDto.getProductName());
        assertEquals(product.getCount(), productDto.getCount());
        assertEquals(product.getPrice(), productDto.getPrice());
    }

    @Test
    public void test_buildEntity() {
        ProductDto productDto = ProductDto.builder()
                .id(1l)
                .productName("Camera")
                .count(2)
                .price(price)
                .build();
        ProductEntity product = PRODUCT_BUILDER.buildEntity(productDto);
        assertEquals(productDto.getId(), product.getId());
        assertEquals(productDto.getProductName(), product.getProductName());
        assertEquals(productDto.getCount(), product.getCount());
        assertEquals(productDto.getPrice(), product.getPrice());
    }
}
