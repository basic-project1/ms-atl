package az.atls.util;

import org.junit.jupiter.api.Test;

import java.util.function.Supplier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class PhoneNumberMaskUtilTest {
    @Test
    void testMaskPhoneNumber_withTrueCase() {
        var phoneNumber = "+994508542160";
        var expected = "+994508****60";
        var result = PhoneNumberMaskUtil.maskPhoneNumber(phoneNumber);
        assertEquals(expected, result);

    }

    @Test
    void testMaskPhoneNumber_withNullValue() {
        String phoneNumber = null;
        assertThrows(IllegalArgumentException.class, ()->PhoneNumberMaskUtil.maskPhoneNumber(phoneNumber));

    }
    @Test
    void testMaskPhoneNumber_withLessCase() {
        var phoneNumber = "+99450854216";
        assertThrows(IllegalArgumentException.class, ()->PhoneNumberMaskUtil.maskPhoneNumber(phoneNumber));

    }
}
