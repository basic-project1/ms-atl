package az.atls.controller;

import az.atls.dao.entity.ProductEntity;
import az.atls.model.dto.ProductDto;
import az.atls.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/products")
@RequiredArgsConstructor
public class ProductController {
    private final ProductService productService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createProduct(@RequestBody ProductDto productDto) {
        productService.createProduct(productDto);
    }

    @GetMapping("/get-products/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ProductDto getProduct(@PathVariable Long id) {
        return productService.getProductById(id);
    }

    @GetMapping("/get-all-products")
    @ResponseStatus(HttpStatus.OK)
    public List<ProductDto> getAllProducts() {
       return productService.getAllProducts();

    }

    @PutMapping("/update-products/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public ProductDto updateProduct(@PathVariable Long id, @RequestBody ProductEntity productEntity) {

        return productService.updateProduct(id, productEntity);

    }

    @DeleteMapping("/delete-product/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteProduct(@PathVariable Long id) {
        productService.deleteProduct(id);
    }

}
