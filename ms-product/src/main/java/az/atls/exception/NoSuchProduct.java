package az.atls.exception;

public class NoSuchProduct extends RuntimeException{
    public NoSuchProduct(String message) {
        super(message);
    }
}
