package az.atls.exception.handler;

import az.atls.consts.ErrorMessage;
import az.atls.exception.NoSuchProduct;
import lombok.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;


import java.time.LocalDateTime;


@RestControllerAdvice

public class ControllerExceptionHandler {
    @ExceptionHandler(NoSuchProduct.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public ErrorMessage resourceNotFoundException(NoSuchProduct ex, WebRequest request) {
        return ErrorMessage.builder()
                .statusCode(HttpStatus.NOT_FOUND.value())
                .timeStamp(LocalDateTime.now())
                .message(ex.getMessage())
                .description(request.getDescription(true)).build();

    }

}

