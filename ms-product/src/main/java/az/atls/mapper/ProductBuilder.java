package az.atls.mapper;

import az.atls.dao.entity.ProductEntity;
import az.atls.model.dto.ProductDto;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum ProductBuilder {
    PRODUCT_BUILDER;

    public final ProductDto buildDto(ProductEntity productEntity) {
        return ProductDto.builder().id(productEntity.getId())
                .productName(productEntity.getProductName())
                .count(productEntity.getCount())
                .price(productEntity.getPrice()).build();

    }

    public final ProductEntity buildEntity(ProductDto productDto) {
        return ProductEntity.builder().id(productDto.getId())
                .productName(productDto.getProductName())
                .count(productDto.getCount())
                .price(productDto.getPrice()).build();

    }

    public final List<ProductDto> buildDtoList(List<ProductEntity> productEntities) {
        List<ProductDto> productDtoList = new ArrayList<>();
        for (int i = 0;i<productEntities.size();i++){
            productDtoList.add(buildDto(productEntities.get(i)));

        }
        return productDtoList;
    }
}
