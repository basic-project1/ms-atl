package az.atls.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public class PhoneNumberMaskUtil {
    private static final String MASK_CHARACTER = "*";
    private static final int VISIBLE_DIGITS_START_INDEX = 7;
    private static final int VISIBLE_DIGITS_END_INDEX = 2;
    private static final int VISIBLE_DIGITS_ALL = 13;
    //+994508****60

    public static String maskPhoneNumber(String phoneNumber) {
        if (phoneNumber == null || phoneNumber.length() < VISIBLE_DIGITS_ALL) {
            throw new IllegalArgumentException("Invalid phone number");
        }
        String visibleDigits = phoneNumber.substring(VISIBLE_DIGITS_START_INDEX,
                VISIBLE_DIGITS_ALL - VISIBLE_DIGITS_END_INDEX);
        String maskedDigits = MASK_CHARACTER.repeat(visibleDigits.length());
        return phoneNumber.substring(0, VISIBLE_DIGITS_START_INDEX) + maskedDigits +
                phoneNumber.substring(11, VISIBLE_DIGITS_ALL);
    }

}
