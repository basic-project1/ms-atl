package az.atls.service;

import az.atls.dao.entity.ProductEntity;
import az.atls.model.dto.ProductDto;

import java.util.List;


public interface ProductService {
    void createProduct(ProductDto productDto);

    ProductDto getProductById(Long id);
    List<ProductDto> getAllProducts();

    ProductDto updateProduct(Long id, ProductEntity productEntity);
    void deleteProduct(Long id);
}
