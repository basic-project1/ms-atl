package az.atls.service.impl;

import static az.atls.mapper.ProductBuilder.PRODUCT_BUILDER;

import az.atls.dao.entity.ProductEntity;
import az.atls.dao.repo.ProductRepository;
import az.atls.exception.NoSuchProduct;
import az.atls.model.dto.ProductDto;
import az.atls.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {
    private final ProductRepository productRepository;

    @Override
    public void createProduct(ProductDto productDto) {
        ProductEntity productEntity = PRODUCT_BUILDER.buildEntity(productDto);
        productRepository.save(productEntity);


    }

    @Override
    public ProductDto getProductById(Long id) {
        return PRODUCT_BUILDER.buildDto(productRepository.findById(id).
                orElseThrow(() -> new NoSuchProduct("Product with this id = " + id + " is not found")));
    }

    @Override
    public List<ProductDto> getAllProducts() {
        return PRODUCT_BUILDER.buildDtoList(productRepository.findAll());
    }


    @Override
    public ProductDto updateProduct(Long id, ProductEntity product) {
        ProductEntity productEntityFromDb = productRepository.findById(id).get();
        productEntityFromDb.setProductName(product.getProductName());
        productEntityFromDb.setCount(product.getCount());
        productEntityFromDb.setPrice(product.getPrice());
        productRepository.save(productEntityFromDb);
        return PRODUCT_BUILDER.buildDto(productEntityFromDb);


    }

    @Override
    public void deleteProduct(Long id) {
        productRepository.deleteById(id);
    }
}
